lerp = (a, b, t) -> a*(1-t)+b*t
clamp = (a,b,x) -> if x < a then a else if x > b then b else x
ilerp = (a, b, p) -> (p - a) / (b - a)
log = (x) -> console.log x

timestring = (rem) ->
  s = (r) -> if Math.round(r) is 1 then "" else "s"
  if rem < 60 then "#{Math.floor(rem)} second#{s(rem)}" 
  else if rem / 60 < 60 then "#{Math.round(rem / 60)} minute#{s(rem/60)}" 
  else if rem / 60 / 60 < 24 then "#{Math.round(rem / 60 / 60)} hour#{s(rem/60/60)}" 
  else if rem / 60 / 60 / 24 < 31 then "#{Math.round(rem / 60 / 60 / 24)} day#{s(rem/60/60/24)}"
  # else if rem / 60 / 60 / 24 / 7 < 4 then "#{Math.round(rem / 60 / 60 / 24 / 7 )} week#{s(rem/60/60/24/7)}"
  else "#{Math.round(rem / 60 / 60 / 24 / 7 / 4 )} month#{s(rem/60/60/24/7/4)}"

class Note
  constructor : (@text, @birth, @death) ->

selected = -1
notes = []

save = (item, data) -> 
  log "saved #{item}"
  localStorage.setItem(item, JSON.stringify(data))
  data

remember = (item, def) ->
  if(!localStorage.getItem(item))
    log "no #{item} found"
    save(item, def)
  else
    log "remembering #{item}"
    JSON.parse(localStorage.getItem(item))

getfuturehopes = () -> {lifetime : $("#lifetime").val(), multiplier:$("#multiplier").val()}
getfeedinghabits = () -> {lifetime : $("#feedamount").val(), multiplier:$("#feedmultiplier").val()}

updateremaining = (n) ->
  now = new Date().getTime()
  remaining = n.death - now
  age = now - n.birth
  $("#remaining").html "#{timestring(remaining / 1000)} left"
  $("#age").html "#{timestring(age / 1000)} "

getcss = (n, now) ->
  ratio = 1 - ilerp(n.birth, n.death, clamp(n.birth, n.death, now))
  c = Math.floor(ratio*255)
  {
    life: {
      height: "#{ratio * 100}%", 
      "background-color":"rgb(#{if c > 50 then 200 else 200}, #{if c > 50 then 200 else 0}, #{if c > 50 then 200 else 0}"
    } 
    text: { 
      height:(if ratio < 0.5 then "#{ilerp(0, 0.5, ratio)*240}px" else "240px")
      opacity : (if ratio < 0.7 then ilerp(0, 0.7, ratio) * 0.6 else "0.6")
    }
  }

animate = (ns) ->
  now = new Date().getTime()
  for n, i in ns
    ratio = 1 - ilerp(n.birth, n.death, clamp(n.birth, n.death, now))
    c = Math.floor(ratio*255)
    css = getcss( n, now)
    $("##{i}life").animate( css.life, 300)
    $("##{i}text").animate( css.text, 300)

newnote = () ->
  if $("#text").val() isnt ""
    birth = new Date().getTime()
    death = birth + $("#lifetime").val() * parseInt($("#multiplier").val()) * 1000
    text = $("#text").val()
    notes.push new Note( text, birth, death)
    log "created '#{text}'"
    save("notes", notes)
    animate(reveal(remember("notes")))
    selected = notes.length - 1
    selectnote(selected)
    $("#text").val("")
    save("lifesettings", getfuturehopes())

feednote = () ->
  notes[selected].death += $("#feedamount").val() * $("#feedmultiplier").val() * 1000
  save("notes", notes)
  updateremaining(notes[selected])
  animate(notes)
  save("feedsettings", getfeedinghabits())
  reveal notes


selectnote = (id) ->
  selected = id
  css = 
    left :$("##{id}note").offset().left
    top : $("##{id}note").offset().top + $("##{id}note").height()# - $("#inspector").height()
    display: "block";
  $("#inspector").animate(css, 300)

reveal = (ns) ->
  $(".note").remove()
  rs = []
  for n, i in ns
    css = getcss(n, new Date().getTime())
    lifetime = $("<div>", {id : "#{i}life", class:"lifetime"})
    lifetime.css css.life 
    text = $("<div>", {id:"#{i}text", class:"text", html:n.text})
    text.css css.text
    nd = $("<div>", {id:"#{i}note", class:"note", html:[ text, lifetime ]})
    nd.mousedown((e) -> selectnote(parseInt(e.currentTarget.id)))
    rs.push {remaining : n.death - new Date().getTime(), dom:nd}

  $("#notes").append rs.sort((a, b) -> a.remaining - b.remaining).map((nn) -> nn.dom)
  ns

murder = (id) ->
  notes[id].death = 0
  cycle( selected )

showthedead = (ds) ->
  $("#dead").html "&#10013 <br>#{ds.length}"
  dds = []
  for d in ds
    dds.push $("<div>", {class:"dead", html:"&#10013<br>" + d.text})
  $("#deadbodies").html dds

cycle = (sel) ->
  dead = []
  removedead = (notes) -> 
    i = 0
    notes.filter((n) -> 
      alive = n.death > new Date().getTime()
      if not alive then dead.push {id: i, note : n}
      i++
      alive
    )
  notes = removedead(notes)
  sel = if notes.length is 0 then -1 else if dead.length > 0 then 0 else sel
  animate notes
  if sel isnt -1
    $("#inspector").css("display", "block")
    selectnote(sel)
    updateremaining(notes[sel])
  else
    $("#inspector").css("display", "none")
  if dead.length > 0
    ds = remember( "dead", [])
    dead.forEach((d) -> ds.push d.note )
    save("dead", ds)
    reveal notes
    save("notes", notes)
    showthedead(ds)
  sel

birth = () ->
  lifesettings = remember("lifesettings", getfuturehopes())
  feedsettings = remember("feedsettings", getfeedinghabits())
  $("#lifetime").val(lifesettings.lifetime)
  $("#multiplier").val(lifesettings.multiplier)
  $("#feedamount").val(feedsettings.lifetime)
  $("#feedmultiplier").val(feedsettings.multiplier)
  $("html").keyup ((e) -> if e.shiftKey and e.keyCode is 13 then newnote())
  $("#murder").click(() -> murder selected)
  $("#feed").click feednote
  $("#switchtheme").click (() -> $('#theme').attr('href', (if $('#theme').attr('href') is 'dark.css' then "light.css" else "dark.css")))

  $("#infotoggle").click (()-> $("#info").css("display", (if $("#info").css("display") is "none" then "block" else "none")))
  $("#add").click newnote
  $("#dead").click (() ->
    $("#deadbodies").css("display", (if $("#deadbodies").css("display") is "none" then "block" else "none"))
    $("#dead").animate({"width" : (if $("#deadbodies").css("display") is "none" then "4em" else "17.3em")}, 300)
    showthedead(remember( "dead", []))
    )
  
  curious = (button, leave, enter) ->
    $("##{button}").mouseleave (() -> $("##{button}").html leave )
    $("##{button}").mouseenter (() -> $("##{button}").html enter )
    
  curious("feed", "++", "feed")
  curious("murder", "x", "murder")
  curious("add", "+", "create")

  notes = remember("notes", [])
  reveal notes
  showthedead (remember "dead", [])
  selected = 0
  cycle(selected)
  stimming = (a) ->
    if document.hasFocus() then selected = cycle( selected)
    window.setTimeout((() -> stimming( not a)), (if a then 1000 else 1000 ))
  stimming(true)

birth()
